import express from 'express'
import bodyParser from 'body-parser';
import cors from 'cors';
import router from './src/routes/routes';
import database from './src/models/database';

//Création de l'app
const app = express();

//configuration du serveur avec Cors: mutli requêtage sur différent appli et BodyParser: parser les reqêtes corses
//  si on ne le mets pas ne parche pas 
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors({origin: true}));

//utilisation des routes/route
app.use('/', router);

//Lancement du serveur 
const port = 3002;

// vérif si la base de donnée est connecté
database()
	.then(async() =>{
		console.log('databaseserver is connected');
		app.listen(port,() =>{
			console.log(`Serveur lancé sur le port ${port}...`);
	});
});


// l'objet app on l'utilise partout pour les route les controleurs
export default app;