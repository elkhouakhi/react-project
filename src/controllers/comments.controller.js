import Comment from "../models/Comment";

class CommentController{
	static async list(request, response){
		let status = 200;
		let body = {};

		try{
			let comments= await Comment.find().populate('userId').populate('voitureId');
			body= {comments, 'message': 'List des comments'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
	
	}
	
	static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let comment= await Comment.create({
			text: request.body.text,
            userId : request.body.userId,
            voitureId: request.body.voitureId
			});
			body= {comment, 'message':'comment created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

	static async details(request, response){
		let status = 200;
		let body = {};
	
		try{
			let id = request.params.id;
			let comment = await Comment.findById(id);	

			body = {comment, 'message': 'Details commentaire'}
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

	static async delete(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			await Comment.deleteOne({_id:id});
			body = {id, 'message': 'supprimer la voiture'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}
    
}

export default CommentController;