import User from "../models/User";

class UserController{
	static async list(request, response){
		let status = 200;
		let body = {};

		try{
			let users= await User.find().populate('idTypeUser');
			body= {users, 'message': 'List des utilisateurs'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
	
	}
	
	static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let user= await User.create({
			nom: request.body.nom,
            prenom : request.body.prenom,
			email: request.body.email,
			password: request.body.password,
			idTypeUser: "5de11e19f526f51b0cbce436", /*request.body.idTypeUser*/
			});
			body= {user, 'message':'user created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);	
	}

	static async details(request, response){
		let status = 200;
		let body = {};
	
		try{
			let id = request.params.id;
			let user = await User.findById(id);	

			body = {user, 'message': 'Details utilisateur'}
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);		
	}


	static async delete(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			await User.deleteOne({_id:id});
			body = {id, 'message': 'supprimer l utilisateur'}; 
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}

	static async update(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			let user= await User.findById(id);
			await user.update(request.body)
			body = {user, 'message': 'modifier user'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}
    
}

export default UserController;