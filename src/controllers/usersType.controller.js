import UserType from "../models/UserType";

class UserTypeController{
	static async list(request, response){
		let status = 200;
		let body = {};

		try{
			let usersType= await UserType.find();
			body= {usersType, 'message': 'List des types utilisateurs'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
	
	}
	
	static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let usersType= await UserType.create({
			idTypeUser: request.body.idTypeUser
			});
			body= {usersType, 'message':'user created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);	
	}
    
}

export default UserTypeController;