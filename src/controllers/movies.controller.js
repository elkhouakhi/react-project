import Movie from "../models/Movie";

class MovieController{

	static async list(request, response){
		let status = 200;
		let body = {};

		try{
			let movies= await Movie.find();
			body= {movies, 'message': 'List des movies'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
	
	}

	static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let movie= await Movie.create({
			name: request.body.name,
            time : request.body.time
			});
			body= {movie, 'message':'movie created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

}

export default MovieController;