import Voiture from "../models/Voiture";

class VoitureController{

	static async list(request, response){
		let status = 200;
		let body = {};

		try{
			let voitures= await Voiture.find();
			body= {voitures, 'message': 'List des voitures'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
	
	}

    static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let voiture= await Voiture.create({
			model: request.body.model,
            prix : request.body.prix,
            photo: request.body.photo,
			});
			body= {voiture, 'message':'voiture created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}   
	static async details(request, response){
		let status = 200;
		let body = {};
	
		try{
			let id = request.params.id;
			let voiture = await Voiture.findById(id);	

			body = {voiture, 'message': 'Details de la voiture'}
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

	static async delete(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			//await Voiture.findById(id);
			await Voiture.deleteOne({_id:id});
			body = {id, 'message': 'supprimer la voiture'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}

	static async update(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			let voiture= await Voiture.findById(id);
			await voiture.update(request.body)
			body = {voiture, 'message': 'Details'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}
}

export default VoitureController;