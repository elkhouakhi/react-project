import {Router} from 'express';
import VoitureController from '../controllers/voitures.controller';
import CommentController from '../controllers/comments.controller';
import UserController from '../controllers/users.controller';
import UserTypeController from '../controllers/usersType.controller';
import PostController from '../controllers/posts.controller';
import MovieController from '../controllers/movies.controller';


const router = Router();

//GET&POST&DELETE&PUT VOITURE
router.get('/voitures', VoitureController.list);
router.post('/voitures', VoitureController.create);
router.get('/voitures/:id', VoitureController.details);
router.delete('/voitures/:id', VoitureController.delete);
router.put('/voitures/:id', VoitureController.update);


//GET&POST&DELETE&PUT COMMENTAIRE
router.get('/comments', CommentController.list);
router.post('/comments', CommentController.create);
router.get('/comments/:id', CommentController.details);
router.delete('/comments/:id', CommentController.delete);

//GET&POST&DELETE&PUT UTILISATEUR
router.get('/users', UserController.list);
router.post('/users', UserController.create);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', UserController.delete);
router.put('/users/:id', UserController.update);

//GET&POST&DELETE&PUT UTILISATEUR types
router.get('/usersType', UserTypeController.list);
router.post('/usersType', UserTypeController.create);

//GET&POST&DELETE&PUT UTILISATEUR types
router.get('/posts', PostController.list);
router.post('/posts', PostController.create);


router.get('/movies', MovieController.list);
router.post('/movies', MovieController.create);

//create
export default router;