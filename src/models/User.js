import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const userSchema = new Schema({
    nom:{
        type: String,
        required: true
    },
    prenom: {
        type: String,
        required: true 
    },
    email: {
        type: String,
        required: true 
    },
    password: {
        type: String,
        required: true 
    },
    idTypeUser: {
        type: Schema.Types.ObjectId,
        ref: 'UserType',
        required: true,
    }

});

const User = model('User', userSchema);

export default User; 
