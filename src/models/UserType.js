import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const userTypeSchema = new Schema({

    idTypeUser: {
        type: String,
        required: true,
    }

});

const UserType = model('UserType', userTypeSchema);

export default UserType; 
