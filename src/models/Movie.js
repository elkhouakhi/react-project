
import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const movieSchema = new Schema(  {
        name: { type: String, required: true },
        time: { type: [String], required: true },
        rating: { type: Number, required: false },
    },
    { timestamps: true });

const Movie = model('Movie', movieSchema);
export default Movie; 
