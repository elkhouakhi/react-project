import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const commentSchema = new Schema({
    text:{
        type: String,
        required: true
    },
    userId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    voitureId: {
        type: Schema.Types.ObjectId,
        ref: 'Voiture',
        required: true,
    }
});

const Comment = model('Comment', commentSchema);

export default Comment; 
