import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const postSchema = new Schema({
    title:{
        type: String,
        required: false
    },
    body: {
        type: String,
        required: false 
    }

});

const Post = model('posts', postSchema);
export default Post; 
