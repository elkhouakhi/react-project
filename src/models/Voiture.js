import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const voitureSchema = new Schema({
    model:{
        type: String,
        required: false
    },
    prix: {
        type: String,
        required: false 
    },
    photo: {
        type: String,
        required: false
    }

});

const Voiture = model('Voiture', voitureSchema);
export default Voiture; 

