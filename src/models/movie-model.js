import {Schema, model} from 'mongoose';
import { truncate } from 'fs';

const Movie = new Schema(
    {
        name: { type: String, required: true },
        time: { type: [String], required: true },
        rating: { type: Number, required: false },
    },
    { timestamps: true },
)


const Moviee = model('movies', Movie);
export default Moviee; 